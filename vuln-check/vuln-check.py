#!/usr/bin/python3


"""
Author: Tyler Higgins
Date 2/25/2020
Made with the help of Violent Python: A Cookbook for Hackers,Forensic Analysts,
Penetration Testers, and Security Engineers By TJ O'Connor
"""


import socket
import sys
import os


def ret_banner(ip, port):
    try:
        socket.setdefaulttimeout(2)
        s = socket.socket()
        s.connect(ip, port)
        banner = s.recv(1024)
        return banner
    except:
        return


def check_vulns(banner):
    f = open("vuln_banners.txt", 'r')
    for line in f.readlines():
        if line.strip('\n') in banner:
            print("[+] Server is vulnerable: " + banner.strip('\n'))

    return


def main():
    if len(sys.argv) == 2:
        filename = sys.argv[1]

        if not os.path.isfile(filename):
            print("[-] " + filename + " does not exist.")
            exit(0)

        if not os.access(filename, os.R_OK):
            print("[-] " + filename + " access denied.")
            exit(0)

        print("[+] Reading Vulnerabilities From: " + filename)

    else:
        print("Usage: " + str(sys.argv[0]) + " <vuln filename>")
        exit(0)

    port_list = [21, 22, 25, 80, 110, 443]
    address = input("Enter the first 3 octets of your IP Address + (.): ")
    for x in range(1, 5):
        ip = address + str(x)
        for port in port_list:
            banner = ret_banner(ip, port)
            if banner:
                print("[+] " + ip + ": " + banner)
                check_vulns(banner)


if __name__ == "__main__":
    main()
