from Login_System.login_gui import *
from Login_System.login import *
from passlib.hash import sha512_crypt

global login_window
global username
global password
global username_entry
global password_entry


def register(window):
    username_info = username.get()
    password_info = sha512_crypt.hash(password.get())

    if username_info and password_info:
        file = open(username_info, 'w')
        file.write(username_info + ":")
        file.write(password_info)
        file.close()
        username_entry.delete(0, END)
        password_entry.delete(0, END)

        Label(window, text="Registration Successful.", fg="green", font=("Calibri", 11)).pack()
    else:
        Label(window, text="Please Provide a Username and Password.", fg="red", font=("Calibri", 11)).pack()


def user_register(window):
    global username
    global password
    global username_entry
    global password_entry
    registration_window = Toplevel(window)
    registration_window.title("Register")
    registration_window.geometry("300x250")

    username = StringVar()
    password = StringVar()

    Label(registration_window, text="Please Enter User Details Below").pack()
    Label(text="").pack()
    Label(registration_window, text="Username *").pack()
    username_entry = Entry(registration_window, textvariable=username)
    username_entry.pack()
    Label(registration_window, text="Password *").pack()
    password_entry = Entry(registration_window, textvariable=password, show="*")
    password_entry.pack()
    Label(text="").pack()
    Button(registration_window, text="Register", width=10, height=1, command=lambda: register(registration_window)).pack()
