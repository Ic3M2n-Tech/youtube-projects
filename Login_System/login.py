import os
from tkinter import *
from Login_System.login_gui import *

global username
global password
global username_entry
global password_entry


def user_login(window):
    username_check = username.get()
    password_check = password.get()

    username_entry.delete(0, END)
    password_entry.delete(0, END)

    file_list = os.listdir()
    if username_check in file_list:
        file = open(username_check, 'r')
        verify = file.readline().split(':')
        hash_pass = sha512_crypt.verify(password_check, verify[1])
        if hash_pass:
            login_success(window)
        else:
            invalid_password(window)
    else:
        no_user(window)


def login_success(window):
    success_window = Toplevel(window)
    success_window.title("Success")
    success_window.geometry("150x100")
    Label(success_window, text="Login Success", fg="green").pack()
    Button(success_window, text="OK", command=lambda: screen_destroy(success_window)).pack()
    session(window)


def invalid_password(window):
    invalid_password_window = Toplevel(window)
    invalid_password_window.title("Invalid Password")
    invalid_password_window.geometry("150x100")
    Label(invalid_password_window, text="Incorrect Password", fg="red").pack()
    Button(invalid_password_window, text="OK", command=lambda: screen_destroy(invalid_password_window)).pack()


def no_user(window):
    no_user_window = Toplevel(window)
    no_user_window.title("No User Found")
    no_user_window.geometry("150x100")
    Label(no_user_window, text="No Matching User Found", fg="red").pack()
    Button(no_user_window, text="OK", command=lambda: screen_destroy(no_user_window)).pack()


def login(window):
    global username
    global password
    global username_entry
    global password_entry
    login_window = Toplevel(window)
    login_window.title("Login")
    login_window.geometry("300x250")

    username = StringVar()
    password = StringVar()
    Label(login_window, text="Please Enter User Details Below to Login").pack()
    Label(text="").pack()
    Label(login_window, text="Username *").pack()
    username_entry = Entry(login_window, textvariable=username)
    username_entry.pack()
    Label(login_window, text="Password *").pack()
    password_entry = Entry(login_window, textvariable=password, show="*")
    password_entry.pack()
    Label(text="").pack()
    Button(login_window, text="Login", width=10, height=1, command=lambda: user_login(window)).pack()
