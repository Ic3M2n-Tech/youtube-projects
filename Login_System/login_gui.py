#!/usr/bin/python3
from Login_System.login import *
from Login_System.register import *

global screen1
global login_window
global username
global password
global username_entry
global password_entry


def screen_destroy(s):
    s.destroy()


def session(window):
    session_window = Toplevel(window)
    session_window.title("Dashboard")
    session_window.geometry("400x400")
    Label(session_window, text="Welcome to the Dashboard").pack()
    Button(session_window, text="Create Note")
    Button(session_window, text="View Note")
    Button(session_window, text="Delete Note")


def main_screen():
    root_window = Tk()
    root_window.geometry("300x250")
    root_window.title("Notes 1.0")  # Change latter

    Label(text="Notes 1.0", bg="gray", width=300, height=2, font=("Calibri", 13)).pack()
    Label(text="").pack()
    Button(text="Login", height=2, width=30, command=lambda: login(root_window)).pack()
    Label(text="").pack()
    Button(text="Register", height=2, width=30, command=lambda: user_register(root_window)).pack()

    root_window.mainloop()


if __name__ == "__main__":
    main_screen()