#! /usr/bin/python3


import zipfile
import argparse
from threading import *

screen_lock = Semaphore(value=1)


def extract_files(z_file, pass_byte):
    try:
        z_file.extractall(pwd=pass_byte)
        screen_lock.acquire()
        print("[+] Found Password: " + str(pass_byte, 'utf-8') + '\n')
    except Exception as e:
        pass
    finally:
        screen_lock.release()


def get_arge():
    example_text = '''example: 

        python3 zip_crack.py -f <zipfile> -d <dictionary.txt>
        python3 zip_crack.py --file <zipfile> --dict <dictionary.txt>'''
    parser = argparse.ArgumentParser(prog='zip_crack.py', description='Brute Force zip archive cracker',
                                     epilog=example_text, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-f", "--file", dest="zip_name", metavar= '', help="Supply the zip file to crack.")
    parser.add_argument("-d", "--dict", dest="dict_name", metavar='', help="Supply the dictionary file.")
    options = parser.parse_args()

    if (not options.zip_name) | (not options.dict_name):
        parser.print_help()
        exit(0)
    else:
        return options


def main():
    options = get_arge()

    zip_name = options.zip_name
    dict_name = options.dict_name
    z_file = zipfile.ZipFile(zip_name)
    pass_file = open(dict_name, 'r')
    for line in pass_file.readlines():
        password = line.strip('\n')
        pass_byte = str.encode(password)
        t = Thread(target=extract_files, args=(z_file, pass_byte))
        t.start()


if __name__ == "__main__":
    main()
