# Function to convert IP address and Subnet Mask to their Binary form.
def to_binary(octet):
    result = octet
    binary = []

    while result != 0:
        if result >= 128:
            result = result - 128
            binary.append('1')
        else:
            binary.append('0')
        if result >= 64:
            result = result - 64
            binary.append('1')
        else:
            binary.append('0')
        if result >= 32:
            result = result - 32
            binary.append('1')
        else:
            binary.append('0')
        if result >= 16:
            result = result - 16
            binary.append('1')
        else:
            binary.append('0')
        if result >= 8:
            result = result - 8
            binary.append('1')
        else:
            binary.append('0')
        if result >= 4:
            result = result - 4
            binary.append('1')
        else:
            binary.append('0')
        if result >= 2:
            result = result - 2
            binary.append('1')
        else:
            binary.append('0')
        if result == 1:
            result = result - 1
            binary.append('1')
        else:
            binary.append('0')

    if not binary:
        binary.append('0')
        binary.append('0')
        binary.append('0')
        binary.append('0')
        binary.append('0')
        binary.append('0')
        binary.append('0')
        binary.append('0')

    return binary


def print_binary_chart(ip_list):
    print("=" * 30)
    print("128\t64\t32\t16\t8\t4\t2\t1")
    for ip in ip_list:
        print(ip + "\t", end="")
    print("\n" + "=" * 30)


def print_octet(octet_list):
    for i in range(0, 4):
        print("Octet " + str(i + 1))
        print_binary_chart(octet_list[i])


def get_bits(binary):
    subnet_bits = 0
    host_bits = 0
    ip_count1 = 0
    while ip_count1 < 4:
        ip_count2 = 0
        for ip in range(0, 8):
            # print(binary[ip_count1][ip_count2])
            if (binary[ip_count1][ip_count2] == '1'):
                subnet_bits = subnet_bits + 1
            else:
                host_bits = host_bits + 1
            ip_count2 = ip_count2 + 1
        ip_count1 = ip_count1 + 1
    return (subnet_bits, host_bits)


# def get_network(binary_ip, binary_subnet):


def host_count(h_bits):
    number_of_hosts = 0
    # Formula to find the number of hosts 2^h -2 = Z
    # (h = number of bits remaining in the host portion of mask, Z = number of hosts available per subnet)
    number_of_hosts = (2 ** h_bits) - 2
    return number_of_hosts


def subnet_count(s_bits, h_bits):
    # Formula to find number of subnets 2^n = Y
    # (n = number of bits used from host portion, Y = the number of subnets)
    number_of_subnets = 0
    number_of_hosts_needed = 0
    added_bits_to_subnet = 0
    print("Current Bits in Host portion: " + str(h_bits))
    number_of_hosts_needed = int(input("How many hosts do you need in each subnet: "))
    number_of_subnets = (2 ** h_bits) // (number_of_hosts_needed + 2)
    # if number_of_subnets == 0:
    #     print("Not enough host bits.")
    #     print(number_of_subnets)

    return number_of_subnets

