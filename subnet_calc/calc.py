#!/usr/bin/python3
from subnet_calc.module import *

"""
Author: Tyler Higgins
Date: 2/16/2020
Version: 1.0
Description: This program will convert an IP address and Subnet Mask to binary and provide the network address,
Host Cout and Max subnet's for a given mask.
"""

# stop = False
subnet_bit = 0
host_bit = 0
max_hosts = 0
max_subnet = 0

print("\nPrinting IP address in Binary\n")
print_octet(binary_ip)

# Creates and stores binary version of subnet mask in a list
print("\nPrinting Subnet Mask in Binary\n")

print_octet(binary_subnet)
(subnet_bit, host_bit) = get_bits(binary_subnet)
print("The number of Bits for the subnet: " + str(subnet_bit))
print('The number of Bits for the host: ' + str(host_bit))
max_hosts = host_count(host_bit)
print("Max Number of Hosts: " + str(max_hosts))
max_subnet = subnet_count(subnet_bit, host_bit)
if max_subnet >= 1:
    print("Max Number of Subnet's: " + str(max_subnet))
else:
    print("Not enough host bits.")
