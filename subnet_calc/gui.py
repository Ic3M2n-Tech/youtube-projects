#! /usr/bin/python3
from tkinter import *
from subnet_calc.module import *

global binary_ip
global binary_subnet


def ip_click():
    global binary_ip
    ip = ip_address.get()
    ip_label = Label(root, text="Your IP Address is: " + ip)
    ip_label.grid(row=0, column=1, sticky="W")
    ip_octet1 = int(ip.split('.')[0])
    ip_octet2 = int(ip.split('.')[1])
    ip_octet3 = int(ip.split('.')[2])
    ip_octet4 = int(ip.split('.')[3])
    binary_ip = [to_binary(ip_octet1), to_binary(ip_octet2), to_binary(ip_octet3), to_binary(ip_octet4)]


def subnet_click():
    global binary_subnet
    mask = subnet_mask.get()
    mask_label = Label(root, text="Your Subnet Mask is: " + mask)
    mask_label.grid(row=1, column=1, sticky="W")
    subnet_octet1 = int(mask.split('.')[0])
    subnet_octet2 = int(mask.split('.')[1])
    subnet_octet3 = int(mask.split('.')[2])
    subnet_octet4 = int(mask.split('.')[3])
    binary_subnet = [to_binary(subnet_octet1), to_binary(subnet_octet2), to_binary(subnet_octet3),
                     to_binary(subnet_octet4)]


root = Tk()
root.title("Subnet Calculator")
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
root_window = "%dx%d" % (screen_width, screen_height)
root.geometry(root_window)

ip_address = Entry(root)
ip_address.grid(row=0, column=0, sticky="W")
ip_submit = Button(root, text="Add IP Address", command=ip_click)
ip_submit.grid(row=0, column=2, sticky="W")

subnet_mask = Entry(root)
subnet_mask.grid(row=1, column=0, sticky="W")
mask_submit = Button(root, text="Add Subnet Mask", command=subnet_click)
mask_submit.grid(row=1, column=2, sticky="W")

root.mainloop()
